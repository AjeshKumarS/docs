# Excel Accounts Backend

[Excel Accounts Backend](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend) manages the back-end functionalities of the  Excel Excel Accounts Service. It's written in [ASP.Net Core 3.1](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-3.1). The project follows Repository pattern. In the root of the repo is a .Net solution folder with 2 project. 

-  API
-  Tests