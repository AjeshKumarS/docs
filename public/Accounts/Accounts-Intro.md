# Excel Accounts Service

[Excel Accounts Service](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Service) is for providing Authentication and to manage user account for different services in Excel Platform.

It has three sub modules

  - See [Excel-Accounts-Auth](./Auth/Accounts-Auth.md) ([Repo](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Auth/))
  - See [Excel-Accounts-Backend](./Backend/Accounts-Backend.md) ([Repo](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Backend))
  - See [Excel-Accounts-Frontend](./Frontend/Accounts-Frontend.md) ([Repo](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Frontend/))

## Setting up Development Environment

### Requirements:

  - You must have [docker](https://docker.com/) installed to run the application
  - To install docker follow this [documentation](https://docker.com/)
  - Direct contribution to the repo is not allowed. So you must first fort the [repo](https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Service).

### Cloning the repository:

  Direct contribution to the repository is not allowed. So you must first fork the repository and then to clone the repository with the sub-modules, do:
  ```sh
$ git clone --recurse-submodules -j8 https://gitlab.com/<YourUsername>/Excel-Account-Service
  ```

### Running the Application:

To run the Application, make sure no processes are running in the following ports in localhost,
  - :80
  - :5000
  - :3000
  - :1000
  - :5432

Then build and run the following commands at the root of the repository
#### Build:

  ```sh
$ docker-compose build
  ```
#### Run:

  ```sh
$ docker-compose up
  ```

#### Run in background:

  ```sh
$ docker-compose up -d
  ```

Verify the application is running by visiting following route in the browser.
```sh
http://localhost
```

You can verify the individual services by visiting the routes
```sh
http://localhost:3000  (Frontend)
http://localhost:5000  (Backend)
```

### Stopping the Application

If you started the application by running the command `docker-compose up` press `Ctrl+c` on windows or `Cmd+c` on mac to stop. If the application is running in background, stop it by running,

```sh
$ docker-compose stop
```

#### To bring all the containers down:

```sh
$ docker-compose down
```



## Setting up remotes

#### Check the current remote:

```sh
$ git remote -v
```

If your fork's URL is listed, then fine. Else:

#### Remove origin:

```sh
$ git remote rm origin
```

#### Set new origin:

```sh
$ git remote add origin https://gitlab.com/<YourUsername>/Excel-Account-Service
```

#### Set upstream:

```sh
$ git remote add upstream https://gitlab.com/excel-mec/excel-accounts/Excel-Accounts-Service
```

(In all these cases, you can use SSH instead of HTTP)
cd into the 3 sub-modules and perform operation similar to step 4 to set proper origin and upstream.

## API Documentation

The back-end API documentation is automatically generated using swagger, you can view it by navigating to the route,
```sh
http://localhost/api/swagger
```

## To-dos

 - Lol lot to do.
