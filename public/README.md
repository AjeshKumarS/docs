# Welcome To Excel Platform

This is the official Documentation for the online platform built as a part of the Techno-Managerial fest Excel of Model Engineering College, Kochi. The platform is build based on micro-service architecture. We have mainly 5 services,

1. [Excel Accounts](Accounts/Accounts-Intro.md):

2. [Excel Play](Play/Play-Intro)

3. [Events Service](Events/Events-Intro)

4. [Prelims](Prelims/Prelims-Intro)

5. [Certificates](Certificates/Certificates-Intro)

   Along with this, we will also have,

   - ​	[Alfred](Alfred/Alfred-Intro.md) - Common Front-end to manage all services 

   - ​	[Excel App](ExcelApp/ExcelApp-Intro.md) - Flutter mobile application

   

   ## Architecture of Excel Platform

   

   ![Excel Platform](assets/ExcelPlatform.png)
